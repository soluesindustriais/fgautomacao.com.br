<div class="cd-hero">
    <ul class="cd-hero-slider autoplay">
        <li class="selected">
            <div class="cd-full-width">
                <h2>Mapeamento de Risco</h2>
                <p>É importante que toda empresa tenha como prioridade a Saúde e Segurança no Trabalho, garantindo o bem-estar dos trabalhadores e a prevenção de acidentes. Para isso, o mapeamento de risco é a...</p>
                <a href="<?=$url?>mapeamento-de-risco" class="cd-btn">Saiba mais</a>
            </div>

        </li>
        <li>
            <div class="cd-full-width">
                <h2>Válvula Solenoide</h2>
                <p>Quando o assunto é válvula solenoide, comumente é descrita como um artefato produzido de latão ou aço inoxidável, bem como com diferentes tipos de classes de isolamento, potência de bobinas... </p>
                <a href="<?=$url?>valvula-solenoide" class="cd-btn">Saiba mais</a>
            </div>

        </li>
        <li>
            <div class="cd-full-width">
                <h2>Inversores de Frequência</h2>
                <p>Sempre que o assunto é inversores de frequência, simplificadamente pode-se descrever como um aparelho produzido de componentes eletrônicos e placas de circuito, que podem ser encontrados em...</p>
                <a href="<?=$url?>inversores-de-frequencia" class="cd-btn">Saiba mais</a>
            </div>

        </li>
    </ul>
    <div class="cd-slider-nav">
        <nav>
            <span class="cd-marker item-1"></span>
            <ul>
                <li class="selected"><a href="#0"><i class="far fa-circle" aria-hidden="true"></i></a></li>
                <li><a href="#0"><i class="far fa-circle" aria-hidden="true"></i></a></li>
                <li><a href="#0"><i class="far fa-circle" aria-hidden="true"></i></a></li>
            </ul>
        </nav>
    </div>

</div>



        <!-- Hero End -->